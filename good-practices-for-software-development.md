#### Question 1

- sharing notes with the team to make sure everyone is on the same page
- turning the video on to improve rapport
- joining the meeting 5-10 minutes early to get to know the team better

#### Question 2

- being more mindful of others in the team
- getting proper exercise
