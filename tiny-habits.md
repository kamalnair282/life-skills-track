#### Question 1
There are only 2 ways to effect long term changes in your own behaviour:
    1. change your environment
    2. make the change tiny enough

#### Question 2

- Behaviour = Motivation + Ability + Prompt
- To make forming new habits easier:
    1. shrink the behaviour; so that you do it even if you have low motivation
    2. identify an action prompt; make it the trigger for the tiny habit you are trying to cultivate
    3. celebrate each tiny win

#### Question 3
Celebrating each win increases motivation and confidence.

#### Question 4
Having clarity and a plan of action is more important than having motivation.

#### Question 5
A change in identity must precede a habit change.

#### Question 6
An action is easier to perform when it is obvious, attractive, easy and satisfying.

#### Question 7
An action is harder to perform when it is invisible, unattractive, hard and unsatisfying.

#### Question 8
I want to read a book everyday. I could keep a book next to my bed so that this reading it becomes obvious to me.

#### Question 9
I want to reduce the time I spend browsing Reddit. I could uninstall the Reddit app on my phone so that browsing Reddit on phone becomes a bit more difficult.
