#### Question 1
1. Avoid getting lost in your own thoughts
2. Try to focus on what the other person is saying
3. Let the other person complete their thoughts before responding
4. Take notes if its an important conversation
5. Paraphrase whatever the other person has said to make sure you understand their point
6. Express interest through words and body language

#### Question 2
- Listen more; talk less
- When you respond, focus on the what is personal to the other person
- Try to clarify and restate what the other person has said
- Try to understand the emotions felt behind what is said
- Try to understand and work with the other's frame of reference
- Respond with acceptance and empathy; don't try to be objective or indifferent; don't fake concern

#### Question 3
- Losing focus and getting lost in your own thoughts
- Finding the conversation boring or unimportant
- Failure to relate with what is being said by the other person

#### Question 4:
- Paraphrasing what is said
- Trying to gain a more comprehensive understanding about what is being said by asking follow-up questions

#### Question 5
- When we feel the stakes are low
- When you feel like making the other person happy is more important

#### Question 6
- When the stakes are high
- When we can't think of any viable solutions

#### Question 7
- When you feel frustrated and powerless
- When you have accepted that there is no way out of a bad situation

#### Question 8
- Identify what you feel yourself; focus on the feeling itself and not what you think about it.
- Identify your real needs; ask yourself why you need this particular thing.
- Try to communicate our actual needs as clearly as possible; try to state them as simple facts.
- Always be aware of your tone and body language.
- Address problems as soon as they arise.
