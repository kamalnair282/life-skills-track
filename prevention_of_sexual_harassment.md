#### What kinds of behaviour cause sexual harassment?
1. verbal:
    - comments about clothing and body
    - sexual or gender based jokes or remarks
    - requesting sexual favours
    - repeatedly asking a person out
    - sexual innuendos, threats, spreading rumours
    - foul and obscene language
2. visual
    - posters, drawings
    - emails, texts of a sexual nature
3. physical
    - sexual assault
    - blocking movement
    - inappropriate touching
    - sexual gesturing
    - leering or staring

#### What would you do in case you face or witness any incident or repeated incidents of such behaviour?
- tell the person to stop
- report it

#### Explains different scenarios enacted by actors.
1. a coworker displays a sexually suggestive poster
2. someone makes a sexual comment about a coworker's body
3. somone overcomplements their coworker when its unwanted
4. an employee thinks their coworker is at a position of advantage because of the nature of the latter's (potentially sexual) relationship with the employer
5. a manager tests the water to find out whether the woman working under him has any personal interest in him
6. someone uses their professional leverage to potentially obtain sexual favours
7. someone keeps asking their coworker out even when the latter person has made it clear that they are not interested
8. someone sends inappropriate jokes to their coworkers
9. someone hugs their coworker even after the latter has made it clear that they are uncomfortable with this gesture
10. someone makes a suggestive joke

#### How to handle cases of harassment?
- document what has happened and what was said
- talk to someone in charge
- report it

#### How to behave appropriately?
- Not saying, doing, displaying or anything that is sexually suggestive in the workplace.
- Make sure you are never doing anything that may potentially cross personal or professional boundaries.
- Always be mindful of how others are responding to your words and actions.

