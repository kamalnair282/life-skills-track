#### 1. Grit
Grit is passion and perseverance for very long-term goals. It is a significant predictor of success. Growth mindset helps to cultivate grit. It is the belief that the ability to learn is not fixed, that it can change with effort.

#### 2. Introduction to Growth Mindset
'Growth mindset' is the belief that skills and intelligence are grown and developed. People who are good at something are good because they built that ability through hard work. It focuses on the process of learning and growing rather than on the outcome.

#### 3. Understanding Internal Locus of Control
Locus of control is the degree to which you believe you have control over your life. To believe that you are in control of your life is the key to staying motivated.

#### 4. How to build Growth Mindset
1. Believe in your ability to figure things out.
2. Question the assumptions that allow you to grow.
3. Develop your own life curriculum.
4. Honour the struggle.

#### 5. Mindset - A MountBlue Warrior Reference Manual
1. Don't be afraid of struggles. Acknowledge them and work through them.
2. Keep reminding yourself that any problem can be solved by putting in the work.
