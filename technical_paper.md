
# Basic datatypes and data structures with code samples in Java

Java, an object-oriented language, uses primitive data types for simple values and composite data structures for complex data management.

Primitive data types in Java are the most basic types that directly hold their values in memory. They are simple and efficient but lack the ability to store multiple values or provide methods for operations on the data. The primitive data types include `byte`, which is an 8-bit integer, and `short`, which is a 16-bit integer. Additionally, there is `int`, a 32-bit integer, and `long`, a 64-bit integer. For floating-point numbers, Java provides `float`, a 32-bit floating-point type, and `double`, a 64-bit floating-point type. The `char` data type represents a 16-bit Unicode character, while the `boolean` data type is used to represent true or false values.

The following code sample shows how a primitive variable is declared and initialized:
```java
int number = 10;
number++;
```

In contrast, composite data types, also known as reference types or objects, are capable of storing multiple values and can include methods to manipulate this data.  Examples of composite data types include classes, arrays, and various types provided by the Java Collections Framework, such as `ArrayList`, `HashSet`, and `HashMap`. Java helps manage composite data types through its  object-oriented programming features. It allows developers to define their own classes, which can encapsulate data and behavior, creating complex data structures. Furthermore, the Collections Framework provides a wide range of pre-built data structures that simplify tasks such as storing and manipulating groups of objects, ensuring efficient and flexible data management beyond the capabilities of primitive types.

The following example shows how objects of reference types are created:
```java
ArrayList<String> list = new ArrayList<>();
list.add("foo");
list.add("bar");
list.add("baz");
```


The data structures provided through the Java Collections Framework include arrays, which store elements of the same type, and `ArrayList`, a resizable array implementation. `LinkedList`, a doubly-linked list, is also available. Java offers `HashSet` and `TreeSet` for storing unique elements and `HashMap` and `TreeMap` for key-value pairs. Other data structures include `Stack`, which follows a last-in, first-out (LIFO) principle, and Queue, which follows a first-in, first-out (FIFO) principle. Additionally, Java provides `PriorityQueue` for elements ordered by their natural ordering or a custom `Comparator` and `Deque` for a double-ended queue.

## References

 * https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html
 * https://docs.oracle.com/javase/specs/jls/se12/html/jls-4.html


