#### Question 1

- Spending time with loved ones
- Eating good food

#### Question 2

- When there is a problem that I have to solve within some given timeframe and I don't know how to go about it
- When I feel like I'm not in control of what is happening or going to happen to me

#### Question 3

- You feel it physically
- You feel powerful and in control

#### Question 4

- Sleep is required for physical and mental wellness.
- You need sleep before and after learning.
- Good quality sleep can slow down age related cognitive decline.
- Regularity is important when it comes to sleep.
- Sleep is a biological necessity; not a luxury.

#### Question 5

- Try to sleep and wake up at the same time everyday
- Avoid naps.
- Reduce caffeine intake.

#### Question 6

- Physical movement has immediate long-lasting and protective benefits for your brain.
- Exercise improves focus, attention, and long-term memory.
- Exercise improves mood and energy as well.
- Exercise protects against age related cognitive decline.
- Minimum exercise required: 3 to 4 30 minutes sessions a week that include aerobics

#### Question 7

- Take stairs whenever you can
- Try to walk more