#### Question 1
To understand something well, teach it or explain it to someone else.

#### Question 2
That relaxing is also an integral part of learning.

#### Question 3
Active mode or focus mode is when you turn your complete attention on something. Diffuse mode is a more relaxed state of mind where you let your mind work in the background in a passive way. Focus mode works best when you are working with thought patterns that are somewhat already familiar to you. New ways of thinking are much more easier to grapple with when your mind is working in diffuse mode.

#### Question 4
1. Decide the skill you want to acquire and break it down into parts. Focus on the parts that will help you reach the goal in a more efficient way.
2. Try to learn just enough to practice and self-correct.
3. Remove all barriers to practicing.
4. Practice at least 20 hours.

#### Question 5
- Be consistent and deliberate with focused learning.
- Remind myself to take breaks while learning.
- Always make sure that I understand the fundamentals well.
- Regularly try to explain the concepts that I learned to my friends.
- Always be mindful about deadlines; make pragmatic decisions based on deadlines.

