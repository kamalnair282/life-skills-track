#### Question 1

Focusing without distractions on a cognitively demanding task.

#### Question 2

1. Schedule distractions at home and at work.
2. Develop a rhythmic deep work ritual.
3. Have a daily evening shutdown ritual.

#### Questions 3

- Plan distractions.
- Avoid context switches to unrelated or emotionally arousing stuff.
- Do deep work followed by shallow work. Have a routine. Do it consistently.
- Try to disconnect completely in the evening by writing the next day's pending work.

#### Questions 4

- Social media is purposely designed to be addictive; it fragments your attention as much as possible throughout your waking hours.
- It can permanently reduce your capacity for concentration.
- The more you use social media the more you are likely to feel lonely or isolated.
- Social media leads to a pervasive background hum of anxiety.
